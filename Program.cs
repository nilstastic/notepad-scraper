﻿[System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

[System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

[System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto, EntryPoint = "SendMessage")]
static extern IntPtr SendMessageSB(IntPtr hWnd, int Msg, int wParam, System.Text.StringBuilder lParam);

const int WM_GETTEXT = 0xd;
const int WM_GETTEXTLENGTH = 0xe;

var getActiveProcesses = () => System.Diagnostics.Process.GetProcessesByName("notepad");

if (getActiveProcesses().Length > 0)
{
    Console.WriteLine($"Found {getActiveProcesses().Length} notepad windows");
}
else
{
    Console.WriteLine($"No notepad windows found, press any key to exit");
    Console.ReadKey();
}

// Save content
Console.WriteLine("\nDo you want me to save the contents to a file? (y/N)");
if (Console.ReadKey().Key.Equals(ConsoleKey.Y))
{
    var fileOutputSb = new System.Text.StringBuilder();

    foreach (var process in getActiveProcesses())
    {
        // Use "Edit" instead of "RichEditD2DPT" on older windows versions
        var hwnd = FindWindowEx(process.MainWindowHandle, IntPtr.Zero, "RichEditD2DPT", "");
        var textLength = SendMessage(hwnd, WM_GETTEXTLENGTH, 0, 0) + 1;
        var windowSb = new System.Text.StringBuilder(textLength);

        if (textLength > 1)
        {
            windowSb.AppendLine(process.MainWindowTitle);
            SendMessageSB(hwnd, WM_GETTEXT, textLength, windowSb);
            windowSb.AppendLine("\n\n-----------------------------------------------------------------\n");
        }

        fileOutputSb.AppendLine(windowSb.ToString());
    }

    var path = $"{AppContext.BaseDirectory}/{DateTime.Now:yyyy-dd-M-HH-mm-ss}.txt";
    File.WriteAllText(path, fileOutputSb.ToString());
    Console.WriteLine($"Data saved to {path}, length {fileOutputSb.Length}\n");
}

// Kill windows
Console.WriteLine("\nDo you want me to close all notepad windows? (y/N)");
if (Console.ReadKey().Key.Equals(ConsoleKey.Y))
{
    var processes = getActiveProcesses();
    Console.WriteLine($"Closing {processes.Length} windows");
    foreach (var process in processes)
    {
        process.Kill();
    }
}

// Exit
Console.WriteLine("\nPress any key to exit");
Console.ReadKey();